+++
# Experience widget.
widget = "experience"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 40  # Order that this section will appear.

title = "Experience"
subtitle = ""

# Date format for experience
#   Refer to https://sourcethemes.com/academic/docs/customization/#date-format
date_format = "Jan 2006"

# Experiences.
#   Add/remove as many `[[experience]]` blocks below as you like.
#   Required fields are `title`, `company`, and `date_start`.
#   Leave `date_end` empty if it's your current employer.
#   Begin/end multi-line descriptions with 3 quotes `"""`.
[[experience]]
  title = "Assistant Professor"
  company = "Federal University of Bahia"
  company_url = "http://ime.ufba.br"
  location = "Bahia - Brazil"
  date_start = "2017-01-01"
  date_end = ""


[[experience]]
  title = "Assistant Professor"
  company = "Fluminense Federal University"
  company_url = "http://ime.uff.br"
  location = "Rio de Janeiro - Brazil"
  date_start = "2016-03-01"
  date_end = "2017-08-31"
  #description = """Taught electronic engineering and researched semiconductor physics."""

[[experience]]
    title = "Analyst"
    company = "Banco Bradesco"
    company_url = "https://banco.bradesco/"
    location = "São Paulo - Brazil"
    date_start = "2011-10-01"
    date_end = "2012-06-30"
    description = """Worked with statistical models for recovery credit."""

[[experience]]
    title = "Analyst"
    company = "Banco Itaú"
    company_url = "https://banco.bradesco/"
    location = "São Paulo - Brazil"
    date_start = "2010-06-01"
    date_end = "2011-01-31"
    description = """Worked with methodological review of statistical models for retail bank."""

+++
